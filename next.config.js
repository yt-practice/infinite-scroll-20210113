new Function('return this')().Element = class ElementMock {}
const basePath =
	'development' === process.env.NODE_ENV ? '' : '/infinite-scroll-20210113/'

module.exports = {
	basePath: basePath.replace(/\/$/, ''),
	assetPrefix: basePath,
	publicRuntimeConfig: {
		basePath,
	},
	trailingSlash: true,
}
