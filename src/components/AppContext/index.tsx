import { useReducer, useContext, createContext } from 'react'
import type { FC, Dispatch } from 'react'
import type { Book as IBook } from '~/pages/api/books'

type Book = IBook & { page?: number }

type Action = { type: 'books:reset' } | { type: 'books:patch'; val: Book[] }
type State = {
	books?: Book[]
}

const initState = (): State => ({})

const StateContext = createContext<State>(initState())
const DispatchContext = createContext<Dispatch<Action>>(() => void 0)

const reducer = (state: State, action: Action): State => {
	switch (action.type) {
		case 'books:patch': {
			const books = new Map(state.books?.map(b => [b.id, b]))
			for (const b of action.val) books.set(b.id, b)
			return {
				...state,
				books: Array.from(books.values()).sort((q, w) => q.id - w.id),
			}
		}
		case 'books:reset':
			return { ...state, books: undefined }
		default:
			// @ts-expect-error: ここには来ない
			throw new Error(`Unknown action: ${action.type}`)
	}
}

export const AppProvider: FC = ({ children }) => {
	const [state, dispatch] = useReducer<typeof reducer>(reducer, initState())
	return (
		<DispatchContext.Provider value={dispatch}>
			<StateContext.Provider value={state}>{children}</StateContext.Provider>
		</DispatchContext.Provider>
	)
}

export const useApp = () => useContext(StateContext)
export const useAppDispatch = () => useContext(DispatchContext)
