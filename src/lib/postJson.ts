// eslint-disable-next-line @typescript-eslint/no-explicit-any
// export const postJson = <V = any>(endpoint: string, body: unknown) =>
// 	fetch(endpoint, {
// 		method: 'post',
// 		headers: { 'content-type': 'application/json' },
// 		body: JSON.stringify(body),
// 	}).then<V>(r => r.json())

import { getBooks, Where } from '~/pages/api/books'

/**
 * API アクセスの mock
 * gitlab pages にアップロードする都合 API が用意できないので
 */
export const postJson = async (endpoint: string, prms: Partial<Where>) => {
	const page = Number(prms.page) | 0
	const id = Number(prms.id) | 0
	await new Promise<void>(r => setTimeout(r, 1000 * Math.random()))
	if (id) return { result: getBooks({ id }) }
	if (page < 1) return { result: [] }
	return { result: getBooks({ page }) }
}
