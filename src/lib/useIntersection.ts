import type { MutableRefObject as Ref } from 'react'
import { useEffect, useState } from 'react'

export const useIntersection = <T extends Element>(ref: Ref<T | null>) => {
	const [intersecting, setIntersecting] = useState(false)
	useEffect(() => {
		const observer = new IntersectionObserver(([entry]) => {
			if (entry) setIntersecting(entry.isIntersecting)
		})
		const current = ref.current
		if (!current) return
		observer.observe(current)
		return () => {
			observer.unobserve(current)
		}
	}, [ref, ref.current])
	return intersecting
}
