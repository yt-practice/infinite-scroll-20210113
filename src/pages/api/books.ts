import type { NextApiHandler } from 'next'
import * as z from 'zod'

const handler: NextApiHandler = async (req, res) => {
	const body = z
		.object({
			page: z.number().int().positive().optional(),
			id: z.number().int().positive().optional(),
		})
		.safeParse(req.body)
	if (!body.success)
		return res.status(400).json({ message: body.error.message })
	const result = getBooks(body.data)
	res.json({ result })
}

export type Book = {
	id: number
	title: string
	description: string
}

export type Where = {
	page: number
	id: Book['id']
}

const makeBook = (id: number): Book => ({
	id,
	title: `testbook_${id}`,
	description: `テスト用本_${id}`,
})
const list = Array.from({ length: 10_000 }, (_, i) => i + 1).map(makeBook)

export const getBooks = (where: Partial<Where>) => {
	const limit = 50
	let tmp = list
	const { id, page } = where
	if (id) return tmp.filter(b => b.id === id)
	if (page && page > 1) tmp = tmp.slice((page - 1) * limit)
	return tmp.slice(0, limit)
}

export default handler
