import '~/styles/main.css'

import NextApp from 'next/app'
import Head from 'next/head'
import { AppProvider } from '~/components/AppContext'

class App extends NextApp {
	render() {
		const { Component, pageProps } = this.props

		return (
			<AppProvider>
				<Head>
					<meta charSet="utf-8" />
					<title>{process.env.NEXT_PUBLIC_SITE_NAME}</title>
					<meta
						name="viewport"
						content="minimum-scale=1,initial-scale=1,width=device-width"
					/>
					<meta
						name="description"
						content={process.env.NEXT_PUBLIC_SITE_DESCRIPTION}
					/>
					<link
						href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@400;700&&display=swap"
						rel="stylesheet"
					/>
					<link
						rel="stylesheet"
						href="https://use.fontawesome.com/releases/v5.0.10/css/all.css"
						integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg"
						crossOrigin="anonymous"
					/>
				</Head>
				<Component {...pageProps} />
			</AppProvider>
		)
	}
}

export default App
