import Link from 'next/link'

const Index = () => {
	return (
		<div>
			<h1>demo</h1>
			<p>
				<Link href="/books">
					<a>book list</a>
				</Link>
			</p>
			<p>
				<Link href="/books#page=2">
					<a>book list page 2</a>
				</Link>
			</p>
			<p>
				<Link href="/books/detail#id=1">
					<a>book id 1</a>
				</Link>
			</p>
		</div>
	)
}

export default Index
