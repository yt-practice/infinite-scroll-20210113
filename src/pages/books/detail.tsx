import { useEffect, useState } from 'react'
import { useApp, useAppDispatch } from '~/components/AppContext'
import { postJson } from '~/lib/postJson'
import NotFound from '~/pages/404'

const gethash = () =>
	new URLSearchParams(('u' > typeof location && location.hash.slice(1)) || '')

const BookPage = () => {
	const { books } = useApp()
	const dispatch = useAppDispatch()
	// const { bookId } = router.query
	const bookId = gethash().get('id') || '0'
	const [fetched, setFetched] = useState(false)
	const [_, reload] = useState(false)
	useEffect(() => {
		const fn2 = () => reload(b => !b)
		window.addEventListener('popstate', fn2)
		return () => {
			window.removeEventListener('popstate', fn2)
		}
	}, [bookId])
	if (bookId && 'string' === typeof bookId && /^\d+$/.test(bookId)) {
		const book = books?.find(b => b.id === Number(bookId))
		if (book) {
			return (
				<div>
					<h1>{book.title}</h1>
					<p>{book.description}</p>
				</div>
			)
		} else if (!fetched) {
			setFetched(true)
			postJson('/api/books', { id: Number(bookId) })
				.then(r => {
					if (!r.result) return Promise.reject(r)
					dispatch({ type: 'books:patch', val: r.result })
				})
				.catch(r => {
					console.error(r)
					alert('データの解析に失敗しました')
				})
		}
	}
	if (fetched) return null
	return <NotFound />
}

export default BookPage
