/* eslint-disable @typescript-eslint/ban-types */
import { useRouter } from 'next/dist/client/router'
import { useEffect, useLayoutEffect, useRef, useState } from 'react'

import Link from 'next/link'
import { useApp, useAppDispatch } from '~/components/AppContext'
import { postJson } from '~/lib/postJson'

import NotFound from '~/pages/404'

const gethash = () =>
	new URLSearchParams(('u' > typeof location && location.hash.slice(1)) || '')

const Books = ({ page }: { page: number }) => {
	const { books } = useApp()
	const dispatch = useAppDispatch()
	useEffect(() => {
		if (page < 1) return
		postJson('/api/books', { page })
			.then(r => {
				if (!r.result) return Promise.reject(r)
				dispatch({
					type: 'books:patch',
					val: r.result.map(b => ({ ...b, page })),
				})
			})
			.catch(r => {
				console.error(r)
				alert('データの解析に失敗しました')
			})
	}, [page])
	return (
		<>
			{(books || [])
				.filter(b => b.page === page)
				.map(b => (
					<li key={b.id} style={{ height: '1.5em' }}>
						<Link href={'/books/detail#id=' + b.id}>
							<a>{b.title}</a>
						</Link>
					</li>
				))}
		</>
	)
}

const BooksPage = () => {
	const router = useRouter()
	const topRef = useRef<HTMLUListElement>(null)
	const midRef = useRef<HTMLUListElement>(null)
	const botRef = useRef<HTMLUListElement>(null)
	// const { paths } = router.query
	// const page = !(paths && Array.isArray(paths) && paths.length)
	// 	? 1
	// 	: 2 === paths.length && 'page' === paths[0] && /^\d+$/.test(paths[1]!)
	// 	? Number(paths[1])
	// 	: 0
	const page = Number(gethash().get('page') || 1)
	const [_, reload] = useState(false)
	useLayoutEffect(() => {
		let jumped = false
		const fn2 = () => reload(b => !b)
		const top = topRef.current
		const mid = midRef.current
		const bot = botRef.current
		if (!(top && mid && bot)) return
		const fn = () => {
			if (jumped) return
			const gap = window.innerHeight / 2
			const re = mid.getBoundingClientRect()
			if (0 === window.pageYOffset || (page < 2 && 0 < re.top)) {
				window.scrollTo(window.pageXOffset, window.pageYOffset + re.top)
				return
			}
			if (gap < re.top && 1 < page) {
				jumped = true
				window.scrollTo(window.pageXOffset, window.pageYOffset + re.height)
				const url = 2 === page ? '/books' : '/books#page=' + (page - 1)
				router.push(url, url, { scroll: false })
			} else if (re.bottom < gap) {
				jumped = true
				window.scrollTo(window.pageXOffset, window.pageYOffset - re.height)
				const url = '/books#page=' + (page + 1)
				router.push(url, url, { scroll: false })
			}
		}
		window.addEventListener('scroll', fn)
		window.addEventListener('popstate', fn2)
		fn()
		return () => {
			window.removeEventListener('scroll', fn)
			window.removeEventListener('popstate', fn2)
		}
	}, [page])

	if (page < 1) {
		return <NotFound />
	}
	return (
		<div>
			<ul style={{ height: '75em' }} ref={topRef}>
				<Books page={page - 1} />
			</ul>
			<ul style={{ height: '75em' }} ref={midRef}>
				<Books page={page} />
			</ul>
			<ul style={{ height: '75em' }} ref={botRef}>
				<Books page={page + 1} />
			</ul>
		</div>
	)
}

export default BooksPage
